# harden-kubernetes

A growing curation used to house information to harden kubernetes deployments.

## Run CIS benchmark
```
sudo docker run --rm -v `pwd`:/host aquasec/kube-bench:latest install
sudo ./kube-bench <your.node>
```

![Finished CIS benchmark](caps/benchmark.png)

_Source:_ https://github.com/aquasecurity/kube-bench


# RBAC best practices (tools):

- [audit2rbac](https://awesomeopensource.com/project/liggitt/audit2rbac):  A tool to automatically detemine what permissions are necessary for certian applaications and can generate RBAC role bindings for you.
- [rbac-manager](https://github.com/FairwindsOps/rbac-manager/blob/master/docs/rbacdefinitions.md): A kubernetes operator that simplifies the management of role bindings and service accounts.
- [kube2iam](https://github.com/jtblin/kube2iam): A tool that provides AWS IAM credentials to containers based on annotations

